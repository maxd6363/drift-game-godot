using DriftGame.Code.Configuration;
using DriftGame.Code.TrackLoader;
using DriftGame.Code.TrackTiles;
using Godot;
using System;

public class TileMapCustom : TileMap
{

	


	public override void _Ready()
	{
		TrackTile[,] track;
		try
		{
			track = new TrackLoaderText().LoadTrack(GameConfiguration.LevelSelectedName);
			for (int i = 0; i < track.GetLength(0); i++)
			{
				for (int j = 0; j < track.GetLength(1); j++)
				{
					if (track[i, j] != TrackTile.NONE)
					{
						SetCell(j, i, TrackTileRealValue(track[i, j]));
						if (track[i, j] == TrackTile.VERTICAL_START)
						{
							GD.Print(Position + new Vector2(j, i) * 384);
							var car = GetParent().GetChild<Car>(0);
							car.Position = Position + new Vector2(j, i) * 384 + Vector2.One * 192 + Vector2.Down * 100;
							car.InitialPosition = car.Position;
						}
					}
					
				}
			}
		}
		catch (Exception e)
		{
			GD.Print(e);
		}


	}

	private int TrackTileRealValue(TrackTile tile)
	{
		switch (tile)
		{
			case TrackTile.NONE: return -1;
			case TrackTile.VERTICAL: return 6;
			case TrackTile.HORIZONTAL: return 5;
			case TrackTile.CORNER_DOWN_RIGHT: return 1;
			case TrackTile.CORNER_DOWN_LEFT: return 0;
			case TrackTile.CORNER_UP_RIGHT: return 3;
			case TrackTile.CORNER_UP_LEFT: return 2;
			case TrackTile.CROSS_SECTION: return 4;
			case TrackTile.VERTICAL_START: return 7;
		}
		return -1;
	}

}
