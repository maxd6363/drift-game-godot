﻿using DriftGame.Code.TrackTiles;

namespace DriftGame.Code.TrackLoader
{
    public abstract class TrackLoader
    {
        public abstract TrackTile[,] LoadTrack(string filename);
    }
}
