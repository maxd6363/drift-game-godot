using DriftGame.Code.TrackTiles;
using Godot;
using System;
using System.IO;


namespace DriftGame.Code.TrackLoader
{
	public class TrackLoaderText : TrackLoader
	{
		public override TrackTile[,] LoadTrack(string filename)
		{
			TrackTile[,] track = null;
			int rows;
			int cols;
			string[] trackData;
			try
			{
				trackData = System.IO.File.ReadAllLines(filename);

				rows = int.Parse(trackData[0].Split(' ')[0]);
				cols = int.Parse(trackData[0].Split(' ')[1]);
				track = new TrackTile[rows, cols];

				for (int i = 0; i < rows; i++)
				{
					string[] lineSplit = trackData[i+1].Split(' ');
					for (int j = 0; j < cols; j++)
					{
						track[i, j] = TrackTileConverter.ConverteFromInteger(int.Parse(lineSplit[j]));
					}
				}

			}
			catch (Exception ex)
			{
				throw ex;
			}
			

			return track;

		}
	}
}
