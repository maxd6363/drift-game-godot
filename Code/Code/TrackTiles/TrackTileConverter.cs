﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DriftGame.Code.TrackTiles
{
    public class TrackTileConverter
    {
        public static TrackTile ConverteFromInteger(int value)
        {
            switch (value)
            {
                case 0: return TrackTile.NONE;
                case 1: return TrackTile.VERTICAL;
                case 2: return TrackTile.HORIZONTAL;
                case 3: return TrackTile.CORNER_DOWN_RIGHT;
                case 4: return TrackTile.CORNER_DOWN_LEFT;
                case 5: return TrackTile.CORNER_UP_RIGHT;
                case 6: return TrackTile.CORNER_UP_LEFT;
                case 7: return TrackTile.CROSS_SECTION;
                case 8: return TrackTile.VERTICAL_START;
            }
            return TrackTile.NONE;
        }
    }
}
