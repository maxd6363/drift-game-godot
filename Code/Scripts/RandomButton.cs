using DriftGame.Code.Configuration;
using DriftGame.Code.TrackGenerator;
using Godot;
using System;

public class RandomButton : TextureButton
{
	private void _on_RandomButton_pressed()
	{
		TrackGenerator generator = new TrackGeneratorFromC();
		generator.GenerateTrack(100,100);
		GetTree().ChangeScene("res://Scenes/Track.tscn");
		GameConfiguration.LevelSelectedName = "TrackGenerator/track1.dat";


	}


}



