using Godot;
using System;

public class Car : KinematicBody2D
{
	private readonly int wheelBase = 128;
	private readonly int enginePower = 1000;
	private readonly float friction = -0.9f;
	private readonly float drag = -0.001f;
	private readonly int brakingPower = -500;
	private readonly int maxSpeedReverse = 250;
	private readonly int slipSpeed = 400;
	private readonly float gripFast = 0.04f;
	private readonly float gripSlow = 0.4f;


	public Vector2 Acceleration { get; private set; }
	public Vector2 Velocity { get; private set; }
	public Vector2 InitialPosition { get; set; }


	private float steerAngle;
	private float turn = 0;
	private bool isRightPressed = false;
	private bool isLeftPressed = false;
	private bool isForwardPressed = false;
	private bool isBackwardPressed = false;


	public override void _Ready()
	{
		base._Ready();
		Position = new Vector2(1000,-1000);
		Acceleration = Vector2.Zero;
		Velocity = Vector2.Zero;
	}

	private int GetSteeringAngle()
	{
		GD.Print(Velocity.Length() * 0.005f);
		return (int)Mathf.Clamp((22 - (Velocity.Length() * 0.004f)), 0, 22) + 1;
	}




	private void HandleInputs()
	{
		isRightPressed = false;
		isLeftPressed = false;
		isForwardPressed = false;
		isBackwardPressed = false;
		if (Input.IsActionPressed("ui_right"))
			isRightPressed = true;
		if (Input.IsActionPressed("ui_left"))
			isLeftPressed = true;
		if (Input.IsActionPressed("ui_up"))
			isForwardPressed = true;
		if (Input.IsActionPressed("ui_down"))
			isBackwardPressed = true;
	}

	private void HandleTurning()
	{
		turn = 0;
		if (isRightPressed)
			turn++;
		if (isLeftPressed)
			turn--;
		steerAngle = turn * Mathf.Deg2Rad(GetSteeringAngle());
	}

	private void HandleAcceleration()
	{
		if (isForwardPressed)
			Acceleration = Transform.x * enginePower;
		if (isBackwardPressed)
			Acceleration = Transform.x * brakingPower;
	}

	private void HandleSteering(float delta)
	{
		var rearWheel = Position - Transform.x * wheelBase / 2.0f;
		var frontWheel = Position + Transform.x * wheelBase / 2.0f;

		rearWheel += Velocity * delta;
		frontWheel += Velocity.Rotated(steerAngle) * delta;
		var newDirection = (frontWheel - rearWheel).Normalized();
		var traction = Velocity.Length() > slipSpeed ? gripFast : gripSlow;
		var braking = newDirection.Dot(Velocity.Normalized());
		if (braking > 0)
			Velocity = Velocity.LinearInterpolate(newDirection * Velocity.Length(), traction);
		if (braking < 0)
			Velocity = Velocity.LinearInterpolate(-newDirection * Mathf.Min(Velocity.Length(), maxSpeedReverse), traction);
		Rotation = newDirection.Angle();
	}

	private void HandleFrictions()
	{
		if (Velocity.Length() < 2)
			Velocity = Vector2.Zero;
		var frictionForce = Velocity * friction;
		var dragForce = Velocity * Velocity.Length() * drag;
		if (Velocity.Length() < 100)
			frictionForce *= 3;
		Acceleration += dragForce + frictionForce;
	}

	public override void _PhysicsProcess(float delta)
	{
		base._PhysicsProcess(delta);
		Acceleration = Vector2.Zero;
		HandleFrictions();
		HandleSteering(delta);
		HandleTurning();
		HandleAcceleration();
		Velocity += Acceleration * delta;
		Velocity = MoveAndSlide(Velocity);
		HandleInputs();
	}


	private void _on_ResetButtom_pressed()
	{
		Velocity = Vector2.Zero;
		steerAngle = 0f;
		Rotation = Mathf.Deg2Rad(-90);
		Position = InitialPosition;
	}

}





