﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DriftGame.Code.TrackGenerator
{
    public abstract class TrackGenerator
    {
        public abstract void GenerateTrack(int numberOfTracks = 1, int rows = 75, int cols = 75);
    }
}
