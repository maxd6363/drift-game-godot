using DriftGame.Code.Configuration;
using DriftGame.Code.TrackGenerator;
using Godot;
using System;

public class LevelSelector : GridContainer
{


	public override void _Ready()
	{
		base._Ready();
		new TrackGeneratorFromC().GenerateTrack(10,55,55);
	}


	private void _on_TextureButton_pressed()
	{
		LoadTrack(1);
	}


	private void _on_TextureButton2_pressed()
	{
		LoadTrack(2);
	}


	private void _on_TextureButton3_pressed()
	{
		LoadTrack(3);
	}


	private void _on_TextureButton4_pressed()
	{
		LoadTrack(4);
	}


	private void _on_TextureButton5_pressed()
	{
		LoadTrack(5);
	}


	private void _on_TextureButton6_pressed()
	{
		LoadTrack(6);
	}


	private void _on_TextureButton7_pressed()
	{
		LoadTrack(7);
	}

	private void _on_TextureButton8_pressed()
	{
		LoadTrack(8);
	}


	private void _on_TextureButton9_pressed()
	{
		LoadTrack(9);
	}


	private void _on_TextureButton10_pressed()
	{
		LoadTrack(10);
	}

	private void LoadTrack(int id)
	{
		GetTree().ChangeScene("res://Scenes/Track.tscn");
		GameConfiguration.LevelSelectedName = $"TrackGenerator/track{id}.dat";

	}

}

