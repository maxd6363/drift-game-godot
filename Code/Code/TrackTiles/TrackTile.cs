﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DriftGame.Code.TrackTiles
{
    public enum TrackTile
    {
        NONE,
        VERTICAL,
        HORIZONTAL,
        CORNER_DOWN_RIGHT,
        CORNER_DOWN_LEFT,
        CORNER_UP_RIGHT,
        CORNER_UP_LEFT,
        CROSS_SECTION,
        VERTICAL_START
    }
}
