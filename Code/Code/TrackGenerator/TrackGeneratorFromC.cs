﻿using DriftGame.Code.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DriftGame.Code.TrackGenerator
{
    public class TrackGeneratorFromC : TrackGenerator
    {
        public override void GenerateTrack(int numberOfTracks = 1, int rows = 75, int cols = 75)
        {
            ToolBox.ExecuteCommandAsync($"TrackGenerator\\trackGenerator.exe {numberOfTracks} {rows} {cols}");
        }
    }
}
