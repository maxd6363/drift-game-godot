﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DriftGame.Code.Utils
{
    public class ToolBox
    {
        public static void ExecuteCommandAsync(string command)
        {
            try
            {
                new Thread(new ParameterizedThreadStart(ExecuteCommandSync))
                {
                    IsBackground = true,
                    Priority = ThreadPriority.AboveNormal
                }.Start(command);
            }
            catch (Exception)
            {
                throw;
            }
        }


        public static void ExecuteCommandSync(object command)
        {
            try
            {
                System.Diagnostics.ProcessStartInfo procStartInfo = new System.Diagnostics.ProcessStartInfo("cmd", command as string)
                {
                    UseShellExecute = false,
                    CreateNoWindow = true
                };
                System.Diagnostics.Process proc = new System.Diagnostics.Process
                {
                    StartInfo = procStartInfo
                };
                proc.Start();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
