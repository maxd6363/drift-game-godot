using Godot;
using System;

public class Camera2D : Godot.Camera2D
{
    private Car car;
    private readonly float zoomSmooth = 2f;
    private readonly float zoomFactor = 900f;
    private readonly float zoomMax = 3f;


    public override void _Ready()
    {
        car = GetParent<Car>();
    }

    public override void _Process(float delta)
    {
        base._Process(delta);
        if (car != null)
        {
            Zoom = Zoom.LinearInterpolate(Vector2.One * (1 + car.Velocity.Length() / zoomFactor), delta * zoomSmooth);
            Zoom = Zoom.Clamped(zoomMax);
        }
    }
}
