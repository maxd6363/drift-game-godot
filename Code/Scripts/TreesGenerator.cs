using Godot;
using System;

public class TreesGenerator : TileMap
{
	public override void _Ready()
	{
		int size = 150;
		float percentTree = 0.05f;
		Random random = new Random();

		for (int i = 0; i < size; i++)
		{
			for (int j = 0; j < size; j++)
			{
				if(random.NextDouble() < percentTree)
				{
					SetCell(i, j, random.Next(5));
				}
			}
		}
		
	}

}
